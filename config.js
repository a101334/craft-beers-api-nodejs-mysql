const CONFIG = {
    baseURL: 'localhost',
    port: 3000,
}

const CONFIG_DB = {
    host: 'localhost',
    user: 'developer',
    password: 'developer',
    dbName: 'craft_beers',
    tableNameBeer: 'beers'
}

module.exports = {
    CONFIG,
    CONFIG_DB
}