const mysql = require('mysql');
const { CONFIG_DB } = require('../config'); 

const { 
    host,
    user,
    password
} = CONFIG_DB;

const genericDB = {
    getConnectionDB: function () {
        const con = mysql.createConnection({
            host,
            user,
            password
        });

        return con;
    }
}

module.exports = {
    genericDB
}
