DROP DATABASE  IF EXISTS craft_beers;
CREATE DATABASE craft_beers;

USE craft_beers;

DROP TABLE  IF EXISTS craft_beers.beers;
CREATE TABLE craft_beers.beers (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	ingredients VARCHAR(50) NOT NULL,
	alcoholContent VARCHAR(50) NOT NULL,
	price DECIMAL(5,2) NOT NULL,
	category VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);