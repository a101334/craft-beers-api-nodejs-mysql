const { genericDB } = require('../model/dbConfig');
const { CONFIG_DB } = require('../config');
const { ERROR_MESSAGES } = require('../constants');

const { 
    MSG_STATUS_400,
    MSG_STATUS_404_BEER_NOT_FOUND,
    MSG_STATUS_500,
    MSG_STATUS_501,
    MSG_STATUS_502
} = ERROR_MESSAGES;

const { 
    dbName,
    tableNameBeer
} = CONFIG_DB;

const beerController = {
    
    saveBeer: function(body) {
        return new Promise((resolve, reject) => {
            try {

                if (this.isValidBody(body)) {
                    const con = genericDB.getConnectionDB();

                    con.connect(function(err) {
                        if (!err) {
                            sqlInsert = `INSERT INTO ${dbName}.${tableNameBeer} (name, ingredients, alcoholContent, price, category) \
                            VALUES ('${body.name}', '${body.ingredients}', '${body.alcoholContent}', ${body.price}, '${body.category}')`;

                            con.query(sqlInsert, function (err, result) {
                                if (!err) {
                                    resolve({
                                        status:201
                                    });
                                } else {
                                    console.log(MSG_STATUS_502);
                                    resolve({
                                        status:502,
                                        body: JSON.stringify({
                                            message: MSG_STATUS_500
                                        })
                                    });
                                };
                            });
                        } else {
                            console.log(MSG_STATUS_501);
                            resolve({
                                status:501,
                                body: JSON.stringify({
                                    message: MSG_STATUS_500
                                })
                            });
                        }; 
                    });

                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: MSG_STATUS_400
                        })
                    });
                }

            } catch(e) {
                console.log(MSG_STATUS_500);
                resolve({
                    status:500,
                    body: JSON.stringify({
                        message: MSG_STATUS_500
                    })
                });
            }
        });
    },
    
    getBeer: function(params) {
        return new Promise((resolve, reject) => {
            try {

                if (this.isValidParameter(params)) {
                    const con = genericDB.getConnectionDB();

                    con.connect(function(err) {
                        if (!err) {
                            sqlInsert = `SELECT * FROM ${dbName}.${tableNameBeer} WHERE id = ${params.id}`;

                            con.query(sqlInsert, function (err, result) {
                                if (!err) {
                                    resolve({
                                        status:200,
                                        body: result
                                    });
                                } else {
                                    console.log(MSG_STATUS_502);
                                    resolve({
                                        status:502,
                                        body: JSON.stringify({
                                            message: MSG_STATUS_500
                                        })
                                    });
                                };
                            });
                        } else {
                            console.log(MSG_STATUS_501);
                            resolve({
                                status:501,
                                body: JSON.stringify({
                                    message: MSG_STATUS_500
                                })
                            });
                        }; 
                    });

                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: MSG_STATUS_400
                        })
                    });
                }

            } catch(e) {
                console.log(MSG_STATUS_500);
                resolve({
                    status:500,
                    body: JSON.stringify({
                        message: MSG_STATUS_500
                    })
                });
            }
        });
    },

    getAllBeers: function() {
        return new Promise((resolve, reject) => {
            try {

                const con = genericDB.getConnectionDB();

                con.connect(function(err) {
                    if (!err) {
                        sqlInsert = `SELECT * FROM ${dbName}.${tableNameBeer}`;

                        con.query(sqlInsert, function (err, result) {
                            if (!err) {
                                resolve({
                                    status:200,
                                    body: result
                                });
                            } else {
                                console.log(MSG_STATUS_502);
                                resolve({
                                    status:502,
                                    body: JSON.stringify({
                                        message: MSG_STATUS_500
                                    })
                                });
                            };
                        });
                    } else {
                        console.log(MSG_STATUS_501);
                        resolve({
                            status:501,
                            body: JSON.stringify({
                                message: MSG_STATUS_500
                            })
                        });
                    }; 
                });

            } catch(e) {
                console.log(MSG_STATUS_500);
                resolve({
                    status:500,
                    body: JSON.stringify({
                        message: MSG_STATUS_500
                    })
                });
            }
        });
    },

    updateBeer: function(params, body) {
        return new Promise((resolve, reject) => {
            try {

                if (this.isValidBody(body) && this.isValidParameter(params)) {
                    const con = genericDB.getConnectionDB();

                    con.connect(function(err) {
                        if (!err) {
                            sqlUpdate = `UPDATE ${dbName}.${tableNameBeer} SET name = '${body.name}', ingredients = '${body.ingredients}', alcoholContent = '${body.alcoholContent}', price = ${body.price}, category = '${body.category}' WHERE id = ${params.id}`;

                            con.query(sqlUpdate, function (err, result) {
                                if (!err) {
                                    if(result.affectedRows > 0) {
                                        resolve({
                                            status:200
                                        });
                                    } else {
                                        resolve({
                                            status:404,
                                            body: JSON.stringify({
                                                message: MSG_STATUS_404_BEER_NOT_FOUND
                                            })
                                        });
                                    }
                                    
                                } else {
                                    console.log(MSG_STATUS_502);
                                    console.log(err);
                                    resolve({
                                        status:502,
                                        body: JSON.stringify({
                                            message: MSG_STATUS_500
                                        })
                                    });
                                };
                            });
                        } else {
                            console.log(MSG_STATUS_501);
                            resolve({
                                status:501,
                                body: JSON.stringify({
                                    message: MSG_STATUS_500
                                })
                            });
                        }; 
                    });

                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: MSG_STATUS_400
                        })
                    });
                }

            } catch(e) {
                console.log(MSG_STATUS_500);
                resolve({
                    status:500,
                    body: JSON.stringify({
                        message: MSG_STATUS_500
                    })
                });
            }
        });
    },

    deleteBeer: function(params) {
        return new Promise((resolve, reject) => {
            try {

                if (this.isValidParameter(params)) {
                    const con = genericDB.getConnectionDB();

                    con.connect(function(err) {
                        if (!err) {
                            sqlInsert = `DELETE FROM ${dbName}.${tableNameBeer} WHERE id = ${params.id}`;

                            con.query(sqlInsert, function (err, result) {
                                if (!err) {
                                    resolve({
                                        status:204,
                                        body: result
                                    });
                                } else {
                                    console.log(MSG_STATUS_502);
                                    resolve({
                                        status:502,
                                        body: JSON.stringify({
                                            message: MSG_STATUS_500
                                        })
                                    });
                                };
                            });
                        } else {
                            console.log(MSG_STATUS_501);
                            resolve({
                                status:501,
                                body: JSON.stringify({
                                    message: MSG_STATUS_500
                                })
                            });
                        }; 
                    });

                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: MSG_STATUS_400
                        })
                    });
                }

            } catch(e) {
                console.log(MSG_STATUS_500);
                resolve({
                    status:500,
                    body: JSON.stringify({
                        message: MSG_STATUS_500
                    })
                });
            }
        });
    },

    isValidBody: function (body) {
        if (body && body.name && body.ingredients && body.alcoholContent && body.price && body.category) {
            return true;
        } else {
            return false;
        }
    },

    isValidParameter: function (params) {
        if (params && params.id) {
            return true;
        } else {
            return false;
        }
    },

}

module.exports = {
    beerController
}