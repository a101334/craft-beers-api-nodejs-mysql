const ERROR_MESSAGES = {
    MSG_STATUS_400: 'missing parameters',
    MSG_STATUS_404_BEER_NOT_FOUND: 'beer not found',
    MSG_STATUS_500: 'snternal server error',
    MSG_STATUS_501: 'san not connect database',
    MSG_STATUS_502: 'sql query error'
};

module.exports = {
    ERROR_MESSAGES
}

