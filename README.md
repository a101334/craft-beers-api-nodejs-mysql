# Project Title

API Craft Beers

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Installed nodeJs, Postman and Mysql
run the script in path model/scripSQL/script_craft_beers.sql

### Installing

Enter root folder, run the comands:
npm i 
node index.js  


## Tip
import the postman file in the root folder Craft Beers.postman_collection.json

### References
https://bitbucket.org/a101334/backend-aws-infra-simulate/src/master/
https://www.w3schools.com/nodejs/nodejs_mysql.asp
https://www.w3schools.com/sql/sql_update.asp
https://expressjs.com/en/api.html
https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

## Authors

* **Alex Rafael da Costa** 