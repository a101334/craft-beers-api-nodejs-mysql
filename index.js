const express = require('express');
const bodyParser = require('body-parser');
const { beerController } = require('./controllers/beerController');

const {
    CONFIG
} = require('./config');

const { port } = CONFIG;

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/beers', async (req, res) => {
    const response = await beerController.getAllBeers();
    res.status(response.status);
    res.send(response);
});

app.post('/beers', async (req, res) => {
    const response = await beerController.saveBeer(req.body);
    res.status(response.status);
    res.send(response);
});

app.get('/beers/:id', async (req, res) => {
    const response = await beerController.getBeer(req.params);
    res.status(response.status);
    res.send(response);
});

app.put('/beers/:id', async (req, res) => {
    const response = await beerController.updateBeer(req.params, req.body);
    res.status(response.status);
    res.send(response);
});

app.delete('/beers/:id', async (req, res) => {
    const response = await beerController.deleteBeer(req.params);
    res.status(response.status);
    res.send(response);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))